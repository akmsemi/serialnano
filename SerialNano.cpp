#include "SerialNano.h"

#if DEVICE_SERIAL

namespace mbed {

#ifdef TARGET_RBLAB_BLENANO
SerialNano::SerialNano(PinName tx, PinName rx) : RawSerial(tx, rx) {
    // http://d.hatena.ne.jp/licheng/20160624/p1
    serial_set_flow_control(&_serial, (FlowControl)Disabled, tx, rx);
    SerialNano::baud(UART_BAUD_RATE);
    SerialNano::format(8, SerialBase::None, 1);
//    _serial.uart->CONFIG &= ~0x01;      // Disable Hardware Flow Control
//    _serial.uart->PSELRTS = 0xFFFFFFFF;
//    _serial.uart->PSELCTS = 0xFFFFFFFF;
}
#endif // TARGET_RBLAB_BLENANO


#ifdef TARGET_RBLAB_BLENANO2
SerialNano::SerialNano(PinName tx, PinName rx) : Serial(tx, rx) {
    SerialNano::baud(UART_BAUD_RATE);
    SerialNano::format(8, SerialBase::None, 1);
    //SerialNano::set_flow_control(SerialBase::Disabled);
}
#endif // TARGET_RBLAB_BLENANO2


#ifdef TARGET_RAYTAC_MDBT42Q
SerialNano::SerialNano(PinName tx, PinName rx) : Serial(tx, rx) {
	SerialNano::baud(UART_BAUD_RATE);
    SerialNano::format(8, SerialBase::None, 1);
    //SerialNano::set_flow_control(SerialBase::Disabled);
}
#endif // TARGET_RAYTAC_MDBT42Q


} // namespace mbed

#endif // DEVICE_SERIAL


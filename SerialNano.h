#ifndef SERIALNANO_H
#define SERIALNANO_H

#include "akdphwinfo.h"
#include "mbed.h"

#ifdef TARGET_RBLAB_BLENANO
#include "RawSerial.h"
#endif

#if defined(TARGET_RAYTAC_MDBT42Q) || defined(TARGET_RBLAB_BLENANO2)
#include "Serial.h"
#endif

#if DEVICE_SERIAL

namespace mbed{
/**
 * This is workaround to use I2C/SPI and serial together on BLE Nano.
 *
 * @code
 * #include "mbed.h"
 * #include "SerialNano.h"
 * 
 * #define PIN_SERIAL_TX   P0_28
 * #define PIN_SERIAL_RX   P0_29
 *
 * #define I2C_SPEED_100KHZ  100000
 *
 * int main() {
 *     // Initialize serial. This must be done before I2C initialization.
 *     SerialNano serial(PIN_SERIAL_TX, PIN_SERIAL_RX);
 *     serial.baud(115200);
 *
 *     // Initialize I2C
 *     I2C connection(I2C_SDA0, I2C_SCL0);
 *     connection.frequency(I2C_SPEED_100KHZ);
 *
 *     while (true) {
 *         // some i2c work
 *         // ...
 *         serial.printf("I2C data\n");
 *         wait_ms(500);
 *     }
 * }
 * @endcode
 */
#ifdef TARGET_RBLAB_BLENANO
class SerialNano : public RawSerial {
#endif
#if defined(TARGET_RAYTAC_MDBT42Q) || defined(TARGET_RBLAB_BLENANO2)
class SerialNano: public Serial {
#endif // TARGET

public:
    SerialNano(PinName tx, PinName rx);
};

} // namespace mbed

#endif // DEVICE_SERIAL

#endif // SERIALNANO_H
